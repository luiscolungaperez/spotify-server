# Instrutions

## Enviroment variables

- it's necessary declare the variables in a file called **_.env_**
  - These are the necessary variables

```
SPOTIFY_CLIENT_ID=
SPOTIFY_CLIENT_SECRET=
SPOTIFY_CALLBACK_HOST=
```

## Install

- It's necessary to install the dependencies for continues

```
  npm install
```

## Execute

- Execute in local
  - Note\*
    is't necessary install nodemon in global

```
  npm run dev
```

- Execute

```
  npm start
```
