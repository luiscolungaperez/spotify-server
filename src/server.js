require('dotenv').config();
const express = require('express');
const cors = require('cors');
const spotifyWebApi = require('spotify-web-api-node');

const app = express();
const port = 8000;

const credentials = {
  clientId: process.env.SPOTIFY_CLIENT_ID,
  clientSecret: process.env.SPOTIFY_CLIENT_SECRET,
  redirectUri: process.env.SPOTIFY_CALLBACK_HOST,
};

app.use(cors()); // To handle cross-origin requests
app.use(express.json()); // To parse JSON bodies

app.get('/', (req, res) => {
  res.send('Welcome to my spotify api!');
});

app.post('/login', (req, res) => {
  let spotifyApi = new spotifyWebApi(credentials);
  const code = req.body.code;

  spotifyApi
    .authorizationCodeGrant(code)
    .then((data) =>
      res.json({
        accessToken: data.body.access_token,
        refreshToken: data.body.refresh_token,
        expiresIn: data.body.expires_in,
      })
    )
    .catch((err) => {
      console.log('Something went wrong!', err);
      res.sendStatus(400);
    });
});

app.post('/refresh', (req, res) => {
  const refreshToken = req.body.refreshToken;
  let spotifyApi = new spotifyWebApi({
    ...credentials,
    refreshToken,
  });

  spotifyApi
    .refreshAccessToken()
    .then((data) => {
      res.json({
        accessToken: data.body.access_token,
        expiresIn: data.body.expires_in,
      });
    })
    .catch((err) => {
      console.log(err);
      res.sendStatus(400);
    });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
